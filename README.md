# Gazou application

## ! INFORMATION !

Veuillez mettre votre localisation pour que l'application puisse fonctionner correctement, merci 😀 !

## Description

L'application Gazou est une application mobile qui recense les stations d'essences proche de vous, en mettant la géolocalisation, avec une map interactive. Il est possible de voir les informations de la station d'essence en cliquant sur le marker et également de les mettre en favoris. Les stations seront sauvegardé dans le local storage du smartphone. Une barre de recherche provenant de Google est mise à disposition pour pouvoir trouver l'endroit et voir s'il y a de l'essence aux alentours. On peut également filtrer par type d'essence ( Gazole / SP95 / SP98 / GPLc / E10 / E85 ). Il y a 2 navigations : la map et les favoris.

## Fonctionnalités

API station d'essence (opendatasoft) : https://public.opendatasoft.com/explore/dataset/prix_des_carburants_j_7/api/?flg=fr&sort=price_gazole&rows=2&dataChart=eyJxdWVyaWVzIjpbeyJjaGFydHMiOlt7InR5cGUiOiJsaW5lIiwiZnVuYyI6IkFWRyIsInNjaWVudGlmaWNEaXNwbGF5Ijp0cnVlLCJjb2xvciI6IiNGRjUxNUEiLCJ5QXhpcyI6InByaWNlX2dhem9sZSJ9LHsiYWxpZ25Nb250aCI6dHJ1ZSwidHlwZSI6ImxpbmUiLCJmdW5jIjoiQVZHIiwieUF4aXMiOiJwcmljZV9zcDk1Iiwic2NpZW50aWZpY0Rpc3BsYXkiOnRydWUsImNvbG9yIjoiIzFCNjY5OCJ9LHsiYWxpZ25Nb250aCI6dHJ1ZSwidHlwZSI6ImxpbmUiLCJmdW5jIjoiQVZHIiwieUF4aXMiOiJwcmljZV9zcDk4Iiwic2NpZW50aWZpY0Rpc3BsYXkiOnRydWUsImNvbG9yIjoiI0ZDRDIzQiJ9XSwieEF4aXMiOiJ1cGRhdGUiLCJtYXhwb2ludHMiOjUwLCJzb3J0IjoiIiwidGltZXNjYWxlIjoibW9udGgiLCJjb25maWciOnsiZGF0YXNldCI6InByaXhfZGVzX2NhcmJ1cmFudHNfal83Iiwib3B0aW9ucyI6eyJmbGciOiJmciIsInNvcnQiOiJ1cGRhdGUifX19XSwidGltZXNjYWxlIjoiIiwiZGlzcGxheUxlZ2VuZCI6dHJ1ZSwiYWxpZ25Nb250aCI6dHJ1ZSwic2luZ2xlQXhpcyI6dHJ1ZX0%3D

Expo map : react-native-maps

Local storage

Barre de recherche de Google

Localisation

Filtrage par essence

Navigation

Landing page

Redirection vers une application GPS pour avoir l'itinéraire

## Technologies utilisées

Axios

React native

React Native Google Places Autocomplete

React Native Maps

React Native Paper

React Native Picker Select

React Native Web

React Native Safe Area Context

React Navigation

Expo Location

Linking

## Installation

git clone https://gitlab.com/Yutsu/gazou

## Lancer le projet

cd gazou

npm install

npx expo start
