import { useRef, useState } from "react"
import { Animated } from "react-native"

export const usePortalPanelBottomList = () => {
  const fadeContainer = useRef(new Animated.Value(0)).current
  const slideInner = useRef(new Animated.Value(500)).current
  const [isTouchable, setIsTouchable] = useState(false)

  const showPanel = () => {
    setIsTouchable(false)
    Animated.timing(fadeContainer, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true
    }).start()
    Animated.timing(slideInner, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
      delay: 50
    }).start()
    fadeContainer.addListener((value) => {
      if (value && value.value === 1) {
        setIsTouchable(true)
        fadeContainer.removeAllListeners()
      }
    })
  }

  const hidePanel = () => {
    setIsTouchable(false)
    Animated.timing(fadeContainer, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
      delay: 150
    }).start()
    Animated.timing(slideInner, {
      toValue: 500,
      duration: 300,
      useNativeDriver: true,
      delay: 150
    }).start()
  }
  return {
    showPanel,
    hidePanel,
    isTouchable,
    fadeContainer,
    slideInner
  }
}
