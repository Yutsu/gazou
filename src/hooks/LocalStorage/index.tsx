import AsyncStorage from "@react-native-async-storage/async-storage"
import { useEffect, useState } from "react"

export const useFavLocalStorage = () => {
  const [stationFav, setStationFav] = useState<string[]>([])

  // AsyncStorage.clear()
  const toggleStation = (dataPortal: any) => {
    AsyncStorage.setItem("favorisStation", JSON.stringify(stationFav))
      .then(() => {
        let stationArray: any = stationFav
        const index = stationArray
          .map((fav: any) => fav.recordid)
          .indexOf(dataPortal.recordid)

        if (index === -1) {
          stationArray = [...stationFav, dataPortal]
          setStationFav(stationArray)
        } else {
          const stationIndex = stationFav.splice(index, 1)
          stationArray = stationFav.filter(
            (station) => station !== stationIndex[0]
          )
          setStationFav(stationArray)
        }
        AsyncStorage.setItem("favorisStation", JSON.stringify(stationArray))
      })
      .catch((error) => console.error(error))
  }

  const getFavStation = async () => {
    AsyncStorage.getItem("favorisStation")
      .then((data) => {
        if (data !== null) {
          setStationFav(JSON.parse(data))
        }
      })
      .catch((error) => console.error(error))
  }

  const renderIconColorFav = (id: string) => {
    if (
      stationFav.length > 0 &&
      stationFav.map((fav: any) => fav.recordid).includes(id)
    ) {
      return "red"
    } else {
      return "black"
    }
  }

  useEffect(() => {
    getFavStation()
  }, [])

  return {
    stationFav,
    getFavStation,
    toggleStation,
    renderIconColorFav
  }
}
