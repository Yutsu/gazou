import { Dimensions, StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  textContainer: {
    backgroundColor: "white"
  },
  text: {
    fontFamily: "Poppins_600SemiBold",
    fontSize: 28,
    textAlign: "center",
    marginTop: 24
  },
  button: {
    backgroundColor: "#eab963",
    paddingVertical: 20,
    borderRadius: 50,
    width: 140,
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    marginTop: 24
  },
  textButton: {
    color: "white",
    fontFamily: "Poppins_600SemiBold"
  }
})
