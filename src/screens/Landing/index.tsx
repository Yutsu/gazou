import { View, Text, Image, Dimensions, TouchableOpacity } from "react-native"
import React from "react"
import background from "../../../assets/images/background.jpg"
import { styles } from "./Styles"
import { useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack"
import { RouteParams } from "../../components/Router/Type"
import AsyncStorage from "@react-native-async-storage/async-storage"

export const Landing = (): React.ReactElement => {
  const navigation = useNavigation<NativeStackNavigationProp<RouteParams>>()

  const storeData = async (value: boolean) => {
    try {
      await AsyncStorage.setItem("alreadyUser", value.toString())
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <View>
      <Image
        source={background}
        style={{
          width: Dimensions.get("window").width,
          height: Dimensions.get("window").height / 1.5
        }}
      />
      <View
        style={[
          { height: Dimensions.get("window").height },
          styles.textContainer
        ]}
      >
        <Text style={styles.text}>Payez moins cher votre carburant</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate("BottomTab"), storeData(true)
          }}
        >
          <Text style={styles.textButton}>Commencer</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
