import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "column",
    borderBottomColor: "#eab963",
    borderBottomWidth: 1,
    marginBottom: 4,
    marginTop: 12,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 8,
    paddingTop: 4
  },
  imageBrand: {
    width: 80,
    height: 56,
    resizeMode: "contain",
    marginTop: 4
  },
  station: {
    paddingBottom: 8
  },
  infoBrandContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    textAlign: "center"
  },
  cityFav: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    marginBottom: 12
  },
  bookmarkIcon: {
    paddingLeft: 8
  },
  dataPortalContainer: {
    color: "#eab963",
    textTransform: "uppercase",
    textAlign: "center",
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 16
  },
  portal: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  icons: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  delete: {
    paddingLeft: 12
  },
  portalContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    position: "relative",
    marginBottom: 16
  },

  text: {
    marginLeft: 8
  },
  iconCross: {
    marginTop: -4
  },
  containerImageDescription: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%"
  },
  containerDescription: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    marginTop: 8
  },
  containerInfoPrice: {
    marginTop: 8,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around"
  },
  description: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row"
  },
  innerRight: {
    display: "flex",
    flexDirection: "column"
  },
  infoPrice: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    borderRadius: 16,
    borderWidth: 1,
    borderColor: "black",
    padding: 16,
    marginTop: 8,
    marginBottom: 16
  },
  sliderInner: {
    backgroundColor: "white",
    padding: 20,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36
  }
})
