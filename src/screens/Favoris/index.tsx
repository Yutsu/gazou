import { View, Text, ScrollView } from "react-native"
import React, { useCallback } from "react"
import { MaterialIcons } from "@expo/vector-icons"
import { useFocusEffect } from "@react-navigation/native"
import { useFavLocalStorage } from "../../hooks/LocalStorage"
import { TouchableOpacity } from "react-native-gesture-handler"
import Icon from "react-native-vector-icons/Feather"
import { Feather } from "@expo/vector-icons"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import { styles } from "./Styles"
import {
  capitalizeFirstLetter,
  renderImageBrand,
  renderInfoPrice,
  renderUpdateDate
} from "../../components/PortalComponent/Utils"

export const Favoris = (): React.ReactElement => {
  const { stationFav, toggleStation, getFavStation, renderIconColorFav } =
    useFavLocalStorage()

  useFocusEffect(
    useCallback(() => {
      getFavStation()
    }, [])
  )

  const renderStationFav = () => {
    return stationFav.map((fav: any) => {
      const { fields } = fav
      return (
        <View key={fav.recordid} style={styles.container}>
          <View style={styles.cityFav}>
            <View>
              <Text>{capitalizeFirstLetter(fields.address)},</Text>
              <Text>{capitalizeFirstLetter(fields.city)},</Text>
              <Text>{capitalizeFirstLetter(fields.cp)}</Text>
            </View>
            <TouchableOpacity onPress={() => toggleStation(fav)}>
              <Icon
                name="heart"
                size={18}
                color={renderIconColorFav(fav.recordid)}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.infoBrandContainer}>
            <View>
              <Text style={styles.station}>
                {capitalizeFirstLetter(fields.name)}
              </Text>
              <View style={styles.description}>
                <Feather name="bookmark" size={20} color="#eab963" />
                <Text style={styles.bookmarkIcon}>{fields.brand}</Text>
              </View>
              <View style={styles.containerDescription}>
                <MaterialIcons name="update" size={20} color="#eab963" />
                {renderUpdateDate(styles, fav)}
              </View>
              <View style={styles.description}>
                {fields.automate_24_24 && (
                  <View style={styles.containerDescription}>
                    <MaterialCommunityIcons
                      name="hours-24"
                      size={20}
                      color="#eab963"
                    />
                    <Text style={styles.text}>{fields.automate_24_24}</Text>
                  </View>
                )}
              </View>
            </View>
            {renderImageBrand(styles, fields.brand)}
          </View>
          <View style={styles.containerInfoPrice}>
            {renderInfoPrice(styles, "SP95", fields.price_sp95)}
            {renderInfoPrice(styles, "SP98", fields.price_sp98)}
            {renderInfoPrice(styles, "Gasoil", fields.gazole)}
            {renderInfoPrice(styles, "E10", fields.e10)}
            {renderInfoPrice(styles, "gplc", fields.gplc)}
            {renderInfoPrice(styles, "e85", fields.e85)}
          </View>
        </View>
      )
    })
  }

  return <ScrollView>{renderStationFav()}</ScrollView>
}
