export interface Localisation {
  latitude: number
  longitude: number
}

export interface Coordinates {
  latitude: number
  longitude: number
  latitudeDelta: number
  longitudeDelta: number
}
