import * as React from "react"
import { Dimensions } from "react-native"
import MapView, { Marker, Region } from "react-native-maps"
import { Localisation } from "./Type"
import { getDelta } from "./Utils"
import { useEffect, useState, createRef } from "react"
import { Text, View } from "react-native"
import { styles } from "./Styles"
import * as Location from "expo-location"
import axios from "axios"
import { usePortalPanelBottomList } from "../../hooks/Portal"
import { LocationObjectCoords } from "expo-location"
import { Markers } from "../../components/Markers"
import { PortalComponent } from "../../components/PortalComponent"
import { CurrentPosition } from "../../components/Buttons/CurrentPosition"
import { Filter } from "../../components/Buttons/Filter"
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete"
import { ActivityIndicator } from "react-native-paper"
import { GoogleSearchBar } from "../../components/GoogleSearchBar"

export const Home = (): React.ReactElement => {
  const [location, setLocation] = useState<Localisation | Region>()
  const [errorMsg, setErrorMsg] = useState<string>()
  const [focusedMarker, setFocusedMarker] = useState<string>("")
  const [dataApi, setDataApi] = useState([])
  const [dataPortal, setDataPortal] = useState<any>()
  const portal = usePortalPanelBottomList()
  const mapRef = createRef<MapView>()
  const [selectedGas, setSelectedGas] = useState("price_gazole")
  const [markerVisible, setMarkerVisible] = useState<boolean>(false)

  const getLocation = async () => {
    const { status } = await Location.requestForegroundPermissionsAsync()
    if (status !== "granted") {
      setErrorMsg("Permission to access location was denied")
      return
    }
    const location = await Location.getCurrentPositionAsync()

    setLocation({
      latitude: location.coords.latitude,
      longitude: location.coords.longitude
    })

    return location.coords
  }

  const changeDataPortal = (data: any) => {
    portal.showPanel()
    setDataPortal(data)
  }

  const getData = async (
    latitude: number | undefined,
    longitude: number | undefined
  ) => {
    const result = await axios(
      `https://public.opendatasoft.com/api/records/1.0/search/?dataset=prix_des_carburants_j_7&q=&rows=10&geofilter.distance=${latitude}%2C+${longitude}%2C+15000`
    )
    setDataApi(result.data.records)
  }

  const changeRegion = async (location: any) => {
    mapRef.current?.animateToRegion({
      ...(await location),
      latitudeDelta: 0.15,
      longitudeDelta: 0.15
    } as Region)
  }

  useEffect(() => {
    getLocation()
      .then((coord: LocationObjectCoords | undefined) =>
        getData(coord?.latitude, coord?.longitude)
      )
      .catch((e) => console.log(e))
  }, [])

  if (errorMsg) {
    return (
      <View style={styles.container}>
        <Text>
          Si vous voyez ce message, c'est que vous avez validez la permission
          mais vous n'avez pas activer la localisation. L'application permet de
          voir les stations d'essences autour de vous, il est donc essentiel de
          l'activer. Veuillez la mettre s'il vous plaît 😉
        </Text>
      </View>
    )
  }

  if (!location) {
    return (
      <View style={[styles.containerSpinner, styles.horizontalSpinner]}>
        <ActivityIndicator size="large" color="#eab963" />
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <GoogleSearchBar
          changeRegion={changeRegion}
          setLocation={setLocation}
          setMarkerVisible={setMarkerVisible}
        />
        <Filter gas={selectedGas} setGas={setSelectedGas} />
      </View>
      <MapView
        style={{
          width: Dimensions.get("window").width,
          height: Dimensions.get("window").height
        }}
        showsUserLocation
        initialRegion={{
          latitude: location.latitude,
          longitude: location.longitude,
          latitudeDelta: getDelta(location.latitude, location.longitude)
            .latitudeDelta,
          longitudeDelta: getDelta(location.latitude, location.longitude)
            .longitudeDelta
        }}
        onRegionChangeComplete={(region) => {
          getData(region.latitude, region.longitude)
        }}
        showsMyLocationButton
        ref={mapRef}
      >
        <Markers
          data={dataApi}
          changeDataPortal={changeDataPortal}
          gas={selectedGas}
          focusedMarker={focusedMarker}
          setFocusedMarker={setFocusedMarker}
        />
        {markerVisible && <Marker coordinate={location} />}
      </MapView>
      <CurrentPosition
        name="navigation"
        onPress={async () => {
          await changeRegion(getLocation())
          setMarkerVisible(false)
        }}
      />
      <PortalComponent
        dataPortal={dataPortal}
        portal={portal}
        setFocusedMarker={setFocusedMarker}
      />
    </View>
  )
}
