import { Dimensions, StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  container: {
    position: "relative",
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    position: "absolute",
    top: 52,
    zIndex: 1,
    marginHorizontal: 16,
    flexDirection: "row"
  },
  containerSpinner: {
    flex: 1,
    justifyContent: "center"
  },
  horizontalSpinner: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
})
