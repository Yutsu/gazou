import { Image, Text, View } from "react-native"
import { Coordinates } from "./Type"

// longitudeDelta : https://github.com/react-native-maps/react-native-maps/issues/505
export const getDelta = (latitude: number, longitude: number): Coordinates => {
  const oneDegreeOfLatitudeInMeters = 111.32 * 1000

  const latitudeDelta = 10000 / oneDegreeOfLatitudeInMeters
  const longitudeDelta =
    10000 / (oneDegreeOfLatitudeInMeters * Math.cos(latitude * (Math.PI / 180)))

  return {
    latitude,
    longitude,
    latitudeDelta,
    longitudeDelta
  }
}
