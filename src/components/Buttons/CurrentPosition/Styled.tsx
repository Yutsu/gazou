import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  btnUserLocation: {
    width: 60,
    height: 60,
    position: "absolute",
    borderRadius: 50,
    bottom: 132,
    right: 16,
    backgroundColor: "white",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 10
  }
})
