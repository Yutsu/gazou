import { TouchableOpacity } from "react-native"
import React from "react"
import Icon from "react-native-vector-icons/Feather"
import { styles } from "./Styled"
import { HeaderMeapProps } from "./Type"

export const CurrentPosition: React.FC<HeaderMeapProps> = ({
  name,
  onPress
}): React.ReactElement => {
  return (
    <TouchableOpacity style={styles.btnUserLocation} onPress={onPress}>
      <Icon name={name} size={24} />
    </TouchableOpacity>
  )
}
