import { GestureResponderEvent } from "react-native"

export interface HeaderMeapProps {
  name: string
  onPress: ((event: GestureResponderEvent) => void) | undefined
}
