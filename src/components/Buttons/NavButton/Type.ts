import { GestureResponderEvent } from "react-native"

export type NavButtonProps = {
  name: string
  focused: boolean
  onPress: ((event: GestureResponderEvent) => void) | undefined
}
