import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  button: {
    width: 60,
    height: 60,
    backgroundColor: "white",
    borderRadius: 50,
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderWidth: 1,
    borderColor: "#eeeeee"
  },
  focusedButton: {
    backgroundColor: "#eab963",
    borderColor: "#d7ae6a"
  }
})
