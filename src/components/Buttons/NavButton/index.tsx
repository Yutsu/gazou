import { TouchableOpacity } from "react-native"
import React from "react"
import Icon from "react-native-vector-icons/Feather"
import { styles } from "./Styled"
import { NavButtonProps } from "./Type"

export const NavButton: React.FC<NavButtonProps> = ({
  name,
  focused,
  onPress
}): React.ReactElement => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.button, focused && styles.focusedButton]}
    >
      <Icon name={name} size={24} color={focused ? "white" : "black"} />
    </TouchableOpacity>
  )
}
