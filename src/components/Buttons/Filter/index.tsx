import { View } from "react-native"
import React from "react"
import { styles } from "./Styled"
import RNPickerSelect from "react-native-picker-select"
import Icon from "react-native-vector-icons/Feather"

type Props = {
  gas: string
  setGas: (e: string) => void
}

export const Filter: React.FC<Props> = ({ gas, setGas }) => {
  const data = [
    { label: "Gazole", value: "price_gazole" },
    { label: "SP95", value: "price_sp95" },
    { label: "SP98", value: "price_sp98" },
    { label: "GPLc", value: "price_gplc" },
    { label: "E10", value: "price_e10" },
    { label: "E85", value: "price_e85" }
  ]

  return (
    <View style={styles.filter}>
      <RNPickerSelect
        placeholder={{}}
        value={gas}
        onValueChange={(value) => setGas(value)}
        items={data}
      >
        <Icon name={"settings"} size={24} />
      </RNPickerSelect>
    </View>
  )
}
