import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  filter: {
    backgroundColor: "white",
    borderRadius: 8,
    width: 48,
    height: 48,
    display: "flex",
    justifyContent: "center",
    marginLeft: 8,
    paddingLeft: 12
  }
})
