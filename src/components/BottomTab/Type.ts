import { BottomTabNavigationEventMap } from "@react-navigation/bottom-tabs"
import {
  NavigationHelpers,
  ParamListBase,
  TabNavigationState
} from "@react-navigation/native"

export type BarProps = {
  state: TabNavigationState<ParamListBase>
  navigation: NavigationHelpers<ParamListBase, BottomTabNavigationEventMap>
}
