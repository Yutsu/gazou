import { View, SafeAreaView } from "react-native"
import React from "react"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { Home } from "../../screens/Home"
import { NavButton } from "../Buttons/NavButton"
import { styles } from "./Styled"
import { BarProps } from "./Type"
import { Favoris } from "../../screens/Favoris"

export const BottomTab = (): React.ReactElement => {
  const { Navigator, Screen } = createBottomTabNavigator()

  const Bar = ({ state, navigation }: BarProps) => {
    return (
      <SafeAreaView
        style={[styles.container, state.index === 0 && styles.inMap]}
      >
        <View style={styles.bar}>
          <NavButton
            onPress={() => navigation.navigate("Home")}
            name="compass"
            focused={state.index === 0}
          />
          <NavButton
            onPress={() => navigation.navigate("Favoris")}
            name="heart"
            focused={state.index === 1}
          />
        </View>
      </SafeAreaView>
    )
  }

  return (
    <Navigator tabBar={(props: any) => <Bar {...props} />}>
      <Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false
        }}
      />
      <Screen name="Favoris" component={Favoris} />
    </Navigator>
  )
}
