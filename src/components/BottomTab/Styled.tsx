import { Dimensions, StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    height: 112,
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
    borderWidth: 1,
    borderColor: "#eeeeee",
    position: "relative"
  },
  inMap: {
    position: "absolute",
    bottom: 0,
    width: Dimensions.get("window").width
  },
  bar: {
    marginTop: 16,
    marginHorizontal: 32,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around"
  }
})
