import { NavigationContainer } from "@react-navigation/native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import React, { useEffect, useState } from "react"
import AsyncStorage from "@react-native-async-storage/async-storage"
import { Landing } from "../../screens/Landing"
import { BottomTab } from "../BottomTab"

export const Router = () => {
  const { Navigator, Screen } = createNativeStackNavigator()
  const [alreadyUser, setAlreadyUser] = useState<Boolean | undefined>()

  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem("alreadyUser")

      if (value === "true") {
        setAlreadyUser(true)
      } else if (value === null) {
        setAlreadyUser(false)
      }
    } catch (e) {
      console.log(e)
    }
  }

  // AsyncStorage.clear()

  useEffect(() => {
    getData()
  }, [])

  return alreadyUser !== undefined ? (
    <NavigationContainer>
      <Navigator initialRouteName={alreadyUser ? "BottomTab" : "Landing"}>
        <Screen
          name="Landing"
          component={Landing}
          options={{ headerShown: false }}
        />
        <Screen
          name="BottomTab"
          options={{ headerShown: false }}
          component={BottomTab}
        />
      </Navigator>
    </NavigationContainer>
  ) : null
}
