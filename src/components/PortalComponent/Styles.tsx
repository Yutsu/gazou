import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  imageBrand: {
    width: 120,
    height: 72,
    resizeMode: "contain",
    marginRight: 16
  },
  portalAnimated: {
    position: "absolute",
    bottom: 0,
    width: "100%"
  },
  stationText: {
    width: 250
  },
  dataPortalContainer: {
    color: "#eab963",
    textTransform: "uppercase",
    textAlign: "center",
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 16
  },
  portal: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  icons: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  marginBtn: {
    paddingLeft: 12
  },
  portalContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    position: "relative",
    marginBottom: 16
  },
  containerImg: {
    width: "30%"
  },
  text: {
    marginLeft: 8
  },
  iconCross: {
    marginTop: -4
  },
  containerImageDescription: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%"
  },
  containerDescription: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    marginTop: 8
  },
  containerInfoPrice: {
    marginTop: 8,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around"
  },
  description: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row"
  },
  innerRight: {
    display: "flex",
    flexDirection: "column",
    width: "60%"
  },
  infoPrice: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    borderRadius: 16,
    borderWidth: 1,
    borderColor: "black",
    padding: 16,
    marginTop: 8,
    marginBottom: 16
  },
  sliderInner: {
    backgroundColor: "white",
    padding: 20,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36
  }
})
