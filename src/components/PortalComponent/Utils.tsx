import { Image } from "react-native"
import Moment from "moment"
import { Text, View } from "react-native"

export const capitalizeFirstLetter = (dataPortal: any) => {
  return dataPortal[0].toUpperCase() + dataPortal.slice(1).toLowerCase()
}

export const renderUpdateDate = (styles: any, dataPortal: any) => {
  let day
  Moment().diff(dataPortal.fields.update, "days") === 1
    ? (day = "jour")
    : (day = "jours")
  return (
    <Text style={styles.text}>
      {Moment(dataPortal.fields.update).format("DD/MM/YYYY")} -{" "}
      {Moment().diff(dataPortal.fields.update, "days")} {day}
    </Text>
  )
}

export const renderInfoPrice = (styles: any, text: string, fields: any) => {
  if (fields) {
    return (
      <View style={styles.infoPrice}>
        <Text>
          {fields < 0.5 ? (fields * 1000).toFixed(3) : fields.toFixed(3)}
        </Text>
        <Text>{text}</Text>
      </View>
    )
  }
}

export const renderImageBrand = (
  styles: any,
  brand: string
): React.ReactNode => {
  const brandStation = brand.toLowerCase()
  if (brandStation === "total" || brandStation === "total access") {
    return (
      <Image
        source={require("../../../assets/images/total.png")}
        style={styles.imageBrand}
      />
    )
  }
  if (brandStation === "esso" || brandStation === "esso express") {
    return (
      <Image
        source={require("../../../assets/images/esso.png")}
        style={styles.imageBrand}
      />
    )
  }
  if (brandStation === "intermarché") {
    return (
      <Image
        source={require(`../../../assets/images/intermarche.png`)}
        style={styles.imageBrand}
      />
    )
  }
  if (brandStation === "auchan") {
    return (
      <Image
        source={require(`../../../assets/images/auchan.png`)}
        style={styles.imageBrand}
      />
    )
  }
  if (brandStation === "indépendant sans enseigne") {
    return (
      <Image
        source={require(`../../../assets/images/independant.png`)}
        style={styles.imageBrand}
      />
    )
  }
  if (brandStation === "système u" || brandStation === "systeme u") {
    return (
      <Image
        source={require(`../../../assets/images/systemeU.png`)}
        style={styles.imageBrand}
      />
    )
  }
  if (brandStation === "avia") {
    return (
      <Image
        source={require(`../../../assets/images/avia.png`)}
        style={styles.imageBrand}
      />
    )
  }
  if (brandStation === "leclerc") {
    return (
      <Image
        source={require(`../../../assets/images/leclerc.png`)}
        style={styles.imageBrand}
      />
    )
  }
  if (brandStation === "casino") {
    return (
      <Image
        source={require(`../../../assets/images/casino.png`)}
        style={styles.imageBrand}
      />
    )
  }
  if (
    brandStation === "carrefour" ||
    brandStation === "carrefour market" ||
    brandStation === "carrefour contact"
  ) {
    return (
      <Image
        source={require(`../../../assets/images/carrefour.png`)}
        style={styles.imageBrand}
      />
    )
  }
  return (
    <Image
      source={require("../../../assets/images/noImage.jpeg")}
      style={styles.imageBrand}
    />
  )
}
