import { Animated, Text, TouchableOpacity, View } from "react-native"
import { Portal } from "react-native-paper"
import { Feather } from "@expo/vector-icons"
import { styles } from "./Styles"
import { useFocusEffect } from "@react-navigation/native"
import Icon from "react-native-vector-icons/Feather"
import { Entypo } from "@expo/vector-icons"
import { MaterialIcons } from "@expo/vector-icons"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import { PortalComponentProps } from "./Type"
import {
  capitalizeFirstLetter,
  renderImageBrand,
  renderInfoPrice,
  renderUpdateDate
} from "./Utils"
import { useFavLocalStorage } from "../../hooks/LocalStorage"
import { useCallback } from "react"
import { Linking } from "react-native"

export const PortalComponent: React.FC<PortalComponentProps> = ({
  dataPortal,
  portal,
  setFocusedMarker
}) => {
  const { toggleStation, renderIconColorFav, getFavStation } =
    useFavLocalStorage()

  useFocusEffect(
    useCallback(() => {
      getFavStation()
    }, [])
  )

  const redirectMaps = (latitude: number, longitude: number) => {
    Linking.openURL(`maps://?daddr=${latitude},${longitude}`)
  }

  return (
    <Portal>
      <Animated.View
        pointerEvents={portal.isTouchable ? "auto" : "none"}
        style={styles.portalAnimated}
      >
        <Animated.View
          style={{
            transform: [{ translateY: portal.slideInner }],
            backgroundColor: "white",
            padding: 20,
            borderTopLeftRadius: 36,
            borderTopRightRadius: 36
          }}
        >
          {dataPortal && (
            <View>
              <Text style={styles.dataPortalContainer}>
                {dataPortal.fields.address}, {dataPortal.fields.city},{" "}
                {dataPortal.fields.cp}
              </Text>
              <View style={styles.portalContainer}>
                <Text style={styles.stationText}>
                  Station {capitalizeFirstLetter(dataPortal.fields.name)}
                </Text>
                <View style={styles.icons}>
                  <TouchableOpacity
                    onPress={() => {
                      redirectMaps(
                        dataPortal.geometry.coordinates[1],
                        dataPortal.geometry.coordinates[0]
                      )
                    }}
                  >
                    <Icon name="map" size={18} />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => toggleStation(dataPortal)}>
                    <Icon
                      name="heart"
                      size={18}
                      color={renderIconColorFav(dataPortal.recordid)}
                      style={styles.marginBtn}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      portal.hidePanel(), setFocusedMarker("")
                    }}
                  >
                    <Entypo
                      name="cross"
                      size={24}
                      color="black"
                      style={styles.marginBtn}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.containerImageDescription}>
                <View style={styles.containerImg}>
                  {renderImageBrand(styles, dataPortal.fields.brand)}
                </View>
                <View style={styles.innerRight}>
                  <View style={styles.description}>
                    <Feather name="bookmark" size={20} color="#eab963" />
                    <Text style={styles.text}>{dataPortal.fields.brand}</Text>
                  </View>
                  <View style={styles.containerDescription}>
                    <MaterialIcons name="update" size={20} color="#eab963" />
                    {renderUpdateDate(styles, dataPortal)}
                  </View>
                  <View style={styles.description}>
                    {dataPortal.fields.automate_24_24 && (
                      <View style={styles.containerDescription}>
                        <MaterialCommunityIcons
                          name="hours-24"
                          size={20}
                          color="#eab963"
                        />
                        <Text style={styles.text}>
                          {dataPortal.fields.automate_24_24}
                        </Text>
                      </View>
                    )}
                  </View>
                </View>
              </View>
              <View style={styles.containerInfoPrice}>
                {renderInfoPrice(styles, "SP95", dataPortal.fields.price_sp95)}
                {renderInfoPrice(styles, "SP98", dataPortal.fields.price_sp98)}
                {renderInfoPrice(
                  styles,
                  "Gazole",
                  dataPortal.fields.price_gazole
                )}
                {renderInfoPrice(styles, "E10", dataPortal.fields.price_e10)}
                {renderInfoPrice(styles, "gplc", dataPortal.fields.price_gplc)}
                {renderInfoPrice(styles, "e85", dataPortal.fields.price_e85)}
              </View>
            </View>
          )}
        </Animated.View>
      </Animated.View>
    </Portal>
  )
}
