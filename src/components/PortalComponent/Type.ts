export interface PortalComponentProps {
  dataPortal: any
  portal: any
  setFocusedMarker: (data: string) => void
}
