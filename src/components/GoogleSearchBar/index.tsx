import React from "react"
import { Dimensions } from "react-native"
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete"
import { styles } from "./Styles"
import { GoogleSearchBarProps } from "./Type"

export const GoogleSearchBar: React.FC<GoogleSearchBarProps> = ({
  changeRegion,
  setLocation,
  setMarkerVisible
}): React.ReactElement => {
  const API_KEY = ""

  return (
    <GooglePlacesAutocomplete
      placeholder="Rechercher une adresse"
      onPress={(_, details = null) => {
        changeRegion({
          latitude: details?.geometry.location.lat as number,
          longitude: details?.geometry.location.lng as number
        })
        setLocation({
          latitude: details?.geometry.location.lat as number,
          longitude: details?.geometry.location.lng as number
        })
        setMarkerVisible(true)
      }}
      query={{
        key: API_KEY,
        language: "fr",
        components: "country:fr"
      }}
      styles={{
        textInput: styles.searchbar,
        listView: {
          width: Dimensions.get("window").width - 32,
          backgroundColor: "white"
        }
      }}
      fetchDetails={true}
      GooglePlacesSearchQuery={{ rankby: "distance" }}
    />
  )
}
