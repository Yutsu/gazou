import { Region } from "react-native-maps"
import { Localisation } from "../../screens/Home/Type"

export interface GoogleSearchBarProps {
  changeRegion: (location: any) => Promise<void>
  setLocation: (value: Localisation | Region | undefined) => void
  setMarkerVisible: (value: boolean) => void
}
