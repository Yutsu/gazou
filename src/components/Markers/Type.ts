export interface MarkersProps {
  data: any
  changeDataPortal: (data: any) => void
  gas: string
  focusedMarker: string
  setFocusedMarker: any
}
