import { View, Text } from "react-native"
import React, { useState } from "react"
import { Marker } from "react-native-maps"
import { styles } from "./styles"
import { MarkersProps } from "./Type"

export const Markers: React.FC<MarkersProps> = ({
  data,
  changeDataPortal,
  gas,
  focusedMarker,
  setFocusedMarker
}): React.ReactElement => {
  if (!data) {
    return <></>
  }

  const renderText = (fields: any) => {
    if (fields) {
      if (fields < 0.5) {
        return (fields * 1000).toFixed(3)
      } else {
        return fields.toFixed(3)
      }
    }
  }

  return data.map((el: any) => {
    return (
      <Marker
        onPress={() => {
          changeDataPortal(el), setFocusedMarker(el.recordid)
        }}
        key={el.recordid}
        coordinate={{
          latitude: el.geometry.coordinates[1],
          longitude: el.geometry.coordinates[0]
        }}
      >
        <View
          style={[
            styles.marker,
            focusedMarker === el.recordid && styles.focused
          ]}
        >
          <Text>⛽️ {renderText(el.fields[gas])}</Text>
          <View
            style={[
              styles.btnMarker,
              focusedMarker === el.recordid && styles.focused
            ]}
          ></View>
        </View>
      </Marker>
    )
  })
}
