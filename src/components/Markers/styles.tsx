import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  marker: {
    backgroundColor: "white",
    padding: 12,
    borderRadius: 50,
    display: "flex"
  },
  btnMarker: {
    position: "absolute",
    width: 12,
    height: 12,
    backgroundColor: "white",
    bottom: -4,
    left: 16,
    alignSelf: "center",
    display: "flex",
    transform: [{ rotate: "45deg" }]
  },
  focused: {
    backgroundColor: "#eab963",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  }
})
