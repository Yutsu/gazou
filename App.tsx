import {
  useFonts,
  Poppins_600SemiBold,
  Poppins_400Regular
} from "@expo-google-fonts/poppins"
import { StatusBar } from "expo-status-bar"
import { LogBox } from "react-native"
import { View } from "react-native"
import { Provider as PaperProvider } from "react-native-paper"
import { Router } from "./src/components/Router"

export default function App() {
  LogBox.ignoreLogs(["Sending"])
  let [fontsLoaded] = useFonts({
    Poppins_600SemiBold,
    Poppins_400Regular
  })

  if (!fontsLoaded) {
    return null
  }

  return (
    <PaperProvider>
      <View style={{ flex: 1 }}>
        <StatusBar style="auto" />
        <Router />
      </View>
    </PaperProvider>
  )
}
